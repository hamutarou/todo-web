(ns hello-todo.boundary.todo-protocol
  (:require [clj-time.core :as time]
            [clj-time.coerce :as tc]
            [clojure.java.jdbc :as jdbc]
            [hugsql.core :as hugsql])
  (:import [hello_todo.component.todo Todo]))

(hugsql/def-db-fns "sql/todo.sql")

(defprotocol TodoProtocol
  ;; boundary definition
  (todo-insert! [db title body])
  (todo-get-all [db])
  (todo-delete! [db id]))

(extend-protocol TodoProtocol
  Todo
  (todo-insert! [this title body]
    (let [{:keys [db]} this
          item {:title title
                :body body
                :created_at (tc/to-long (time/now))
                :updated_at (tc/to-long (time/now))}]
      (insert-todo! (:spec db) item)))
  (todo-get-all [this]
    (let [{:keys [db]} this]
      (get-todos (:spec db))))
  (todo-delete! [this id]
    (let [{:keys [db]} this]
      (delete-todo! (:spec db) {:id id}))))
          


