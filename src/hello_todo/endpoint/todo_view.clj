(ns hello-todo.endpoint.todo-view
  (:require [compojure.core :refer :all]
            [clojure.java.jdbc :as jdbc]
            [formative.core :as f]
            [formative.parse :as fp]
            [hiccup.core :as h]
            [hiccup.page :as hp]
            [clj-time.core :as time]
            [clj-time.coerce :as tc]
            [clj-time.format :as tf]
            [ring.util.response :as rp]
            [hello-todo.boundary.todo-protocol :as tp]
            [hello-todo.component.todo :as td]
            [selmer.parser :as sp]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]))

(def make-todo-form
  {:fields [{:name :title :type :text}
            {:name :body :type :textarea}]
   :validations [[:required [:title :body]]]})

(defn show-todo [todo & {:keys [problems]}]
  (let [items (tp/todo-get-all todo)
        todo-form (h/html (f/render-form
                           (assoc make-todo-form
                                  :problems problems)))]
    (sp/render-file "hello_todo/endpoint/todo_view/index.html" {:items items
                                                                :todo-form todo-form
                                                                :token *anti-forgery-token*})))

(defn make-todo [todo params]
  (fp/with-fallback #(show-todo todo :problems %)
    (let [values (fp/parse-params make-todo-form params)]
      (tp/todo-insert! todo (:title values) (:body values)))
    (rp/redirect "/todo/")))

(defn delete-todo [todo id]
  (tp/todo-delete! todo id)
  (rp/redirect "/todo/"))

(defn todo-view-endpoint [{:keys [todo] :as config}]
  (context "/todo" []
           (GET "/" [] (show-todo todo))
           (POST "/" {params :params} (make-todo todo params))
           (POST "/:id" [id] (delete-todo todo id))))
