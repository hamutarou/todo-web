(ns hello-todo.component.todo
  (:require [com.stuartsierra.component :as component]))

(defrecord Todo [db])

(defn todo [options]
  (map->Todo options))
