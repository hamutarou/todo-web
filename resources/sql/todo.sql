-- :name insert-todo! :i!
insert into todo  (title, body, created_at, updated_at)
           values (:title, :body, :created_at, :updated_at)

-- :name get-todos :? :*
select * from todo

-- :name delete-todo! :!
delete from todo where id = :id
