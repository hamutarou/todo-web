CREATE TABLE todo (id INTEGER PRIMARY KEY AUTOINCREMENT,
       	     	   title TEXT,
		   body TEXT,
		   created_at INTEGER,
		   updated_at INTEGER);

